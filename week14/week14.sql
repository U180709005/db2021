use company;

create temporary table TopSuppliers(
	SupplierID int primary key,
    SupplierName varchar(45),
    ProductCount int
);

insert into TopSuppliers (SupplierID,SupplierName,ProductCount)
select suppliers.SupplierID, SupplierName, count(productid) as ProductCount
from suppliers join products on suppliers.SupplierID=products.SupplierID
group by suppliername
order by ProductCount desc;

select *
from topsuppliers
where ProductCount>3
order by ProductCount desc;

call check_table_exists('TopSuppliers');
drop temporary table topsuppliers;

select * from customers;

update customers
set CustomerName = "Mehmet Kadri Gofralilar", City = "Mugla", PostalCode = "48100", Country = "Turkey"
where CustomerID=1;

delete from customers
where CustomerID = 1;

truncate table Customers;
delete from Customers;
drop table Customers;