CREATE TABLE `Department` (
  `department_id` int NOT NULL,
  `department_name` varchar(45) NOT NULL,
  PRIMARY KEY (`department_id`)
);

CREATE TABLE `Employee` (
  `employee_id` bigint NOT NULL,
  `employee_name` varchar(45) NOT NULL,
  `gender` char(1) NOT NULL,
  `pay_rate` double NOT NULL,
  `state` char(3) NOT NULL,
  `zip` varchar(8) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `citizen_desc` varchar(45) NOT NULL,
  `hispanic_latino` char(3) NOT NULL,
  `race_desc` varchar(45) NOT NULL,
  `recruiment_source` varchar(45) NOT NULL,
  `engagement_survey` double NOT NULL,
  `employee_satisfaction` int NOT NULL,
  `special_project_counts` int NOT NULL,
  `last_performance_rewiev_date` date DEFAULT NULL,
  `marital_status_id` int NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `marital_status_id_idx` (`marital_status_id`),
  KEY `recruiment_idx` (`recruiment_source`),
  KEY `name_idx` (`employee_name`),
  CONSTRAINT `marital_status_id` FOREIGN KEY (`marital_status_id`) REFERENCES `Married` (`marital_status_id`)
);

CREATE TABLE `EmployeeStatus` (
  `employee_status_id` int NOT NULL,
  `employee_status` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_status_id`)
);

CREATE TABLE `ManagedBy` (
  `employee_id` bigint NOT NULL,
  `manager_id` bigint NOT NULL,
  PRIMARY KEY (`employee_id`,`manager_id`),
  KEY `fk_Employee_has_Manager_Manager1_idx` (`manager_id`),
  KEY `fk_Employee_has_Manager_Employee1_idx` (`employee_id`),
  CONSTRAINT `employee_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `Employee` (`employee_id`),
  CONSTRAINT `manager_id_fk` FOREIGN KEY (`manager_id`) REFERENCES `Manager` (`manager_id`)
);

CREATE TABLE `Manager` (
  `manager_id` bigint NOT NULL,
  `manager_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`manager_id`)
);


CREATE TABLE `Manages` (
  `manager_id` bigint NOT NULL,
  `department_id` int NOT NULL,
  PRIMARY KEY (`manager_id`,`department_id`),
  KEY `fk_Manager_has_Department_Department1_idx` (`department_id`),
  KEY `fk_Manager_has_Department_Manager1_idx` (`manager_id`),
  CONSTRAINT `fk_Manager_has_Department_Department1` FOREIGN KEY (`department_id`) REFERENCES `Department` (`department_id`),
  CONSTRAINT `fk_Manager_has_Department_Manager1` FOREIGN KEY (`manager_id`) REFERENCES `Manager` (`manager_id`)
);

CREATE TABLE `Married` (
  `marital_status_id` int NOT NULL,
  `marital_desc` varchar(45) NOT NULL,
  PRIMARY KEY (`marital_status_id`)
);

CREATE TABLE `PerformanceScore` (
  `performance_score_id` int NOT NULL,
  `performance_score` varchar(45) NOT NULL,
  PRIMARY KEY (`performance_score_id`)
);
CREATE TABLE `Position` (
  `position_id` int NOT NULL,
  `position_name` varchar(45) NOT NULL,
  PRIMARY KEY (`position_id`)
);
CREATE TABLE `PositionHistory` (
    `employee_id` BIGINT NOT NULL,
    `position_id` INT NOT NULL,
    `department_id` INT NOT NULL,
    `date_of_hire` DATE NOT NULL,
    `date_of_termination` DATE DEFAULT NULL,
    `employee_status_id` INT NOT NULL,
    `performance_score_id` INT NOT NULL,
    `term_reason_id` INT NOT NULL,
    PRIMARY KEY (`employee_id` , `position_id` , `department_id`),
    KEY `fk_PositionHistory_Position1_idx` (`position_id`),
    KEY `fk_PositionHistory_Department1_idx` (`department_id` , `employee_status_id` , `performance_score_id` , `term_reason_id`),
    KEY `fk_EmployeeStatus_employee_status1_idx` (`employee_status_id`),
    KEY `fk_PerformanceScore_performance_score1_idx` (`performance_score_id`),
    KEY `fk_TermReason_term_reason_id1_idx` (`term_reason_id`),
    CONSTRAINT `fk_EmployeeStatus_employee_status1` FOREIGN KEY (`employee_status_id`)
        REFERENCES `EmployeeStatus` (`employee_status_id`),
    CONSTRAINT `fk_PerformanceScore_performance_score1` FOREIGN KEY (`performance_score_id`)
        REFERENCES `PerformanceScore` (`performance_score_id`),
    CONSTRAINT `fk_PositionHistory_Position1` FOREIGN KEY (`position_id`)
        REFERENCES `Position` (`position_id`),
    CONSTRAINT `fk_TermReason_term_reason_id1` FOREIGN KEY (`term_reason_id`)
        REFERENCES `TermReason` (`term_reason_id`)
);

CREATE TABLE `TermReason` (
    `term_reason_id` INT NOT NULL,
    `term_reason` VARCHAR(45) DEFAULT NULL,
    PRIMARY KEY (`term_reason_id`)
);



