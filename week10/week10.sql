use company;

UPDATE customers SET Country = REPLACE(Country,'\n','');

select count(CustomerID), Country
from customers
group by Country
order by count(CustomerID) desc;

create view UsaCustomers as
select CustomerName, ContactName
from customers
where Country='USA';

select * from usacustomers;

create or replace view ProductPrice as
select ProductName, Price
from products
where Price > (select avg(Price) from products);

select * from productprice;